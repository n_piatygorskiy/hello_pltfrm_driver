#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/fs.h>   
#include <asm/uaccess.h>
#include <asm/atomic.h>
#include <asm/byteorder.h>
#include <linux/delay.h>
#include <linux/poll.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/miscdevice.h>

#include <linux/of.h>
#define  DEVICE_NAME "pminf"	// The device will appear at /dev/pminf using this value

static int gpiotest_open(struct inode *, struct file *);
static int gpiotest_release(struct inode *, struct file *);
static ssize_t gpiotest_read(struct file *file, char __user *buf, size_t count, loff_t *offset);
static ssize_t gpiotest_write(struct file *file, const char __user *buf, size_t count, loff_t *offset);
static unsigned int gpiotest_poll(struct file *, poll_table *);

static const struct file_operations gpiotest_fops = {
	.open = gpiotest_open,
	.read = gpiotest_read,
	.write = gpiotest_write,
	.release = gpiotest_release,
	.poll = gpiotest_poll,
};

#define MAX_BUF 100
uint8_t *data_read;
int read_pos = 0;

uint8_t *data_write;

static int gpiotest_probe(struct platform_device *pdev);
static int gpiotest_remove(struct platform_device *pdev);

static struct platform_device *gpiotest_device;

static struct platform_driver gpiotest_driver = {
	.probe = gpiotest_probe,
	.remove = gpiotest_remove,
	.driver = {
			.owner = THIS_MODULE,	
			.name = "gpiotest",
		   },
};



static int gpiotest_open(struct inode *inodep, struct file *filep)
{
	return 0;
}

static ssize_t gpiotest_read(struct file *file, char __user *buf, size_t count, loff_t *offset)
{
	size_t datalen = read_pos;

	dev_info(&gpiotest_device->dev, "Reading device: %d\n", MINOR(
				file->f_path.dentry->d_inode->i_rdev));

	if (count > datalen) {
		count = datalen;
	}

	if (copy_to_user(buf, data_read, count)) {
		return -EFAULT;
	} 
	read_pos = 0;

	return count;
}

static ssize_t gpiotest_write(struct file *file, const char __user *buf, size_t count, loff_t *offset)
{
	size_t maxdatalen = MAX_BUF, ncopied;

	dev_info(&gpiotest_device->dev, "Writing device: %d\n", MINOR(
				file->f_path.dentry->d_inode->i_rdev));

	if (count < maxdatalen) {
		maxdatalen = count;
	}
	ncopied = copy_from_user(data_write, buf, maxdatalen);
	
	if (ncopied == 0) {
		dev_info(&gpiotest_device->dev, "Copied %zd bytes from the user\n",
				maxdatalen);
	} else {
		dev_info(&gpiotest_device->dev, "Could not copy %zd bytes from the user\n",
				maxdatalen);
	}

	data_write[maxdatalen] = 0;
	read_pos = snprintf(data_read, MAX_BUF, "Hello, %s", data_write);
	dev_info(&gpiotest_device->dev, "Data from the user: %s\n", data_write);

	return count;
}

static unsigned int gpiotest_poll(struct file *filep, poll_table *wait)
{
	return 0;
}

static int gpiotest_release(struct inode *inodep, struct file *filep)
{
	return 0;
}

static struct miscdevice gpiotest_misc_dev = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = DEVICE_NAME,
	.fops = &gpiotest_fops,
	.mode =  S_IRWXUGO,
};

static int gpiotest_probe(struct platform_device *pdev)
{
	int ret;
	dev_info(&pdev->dev, ": probe called");

	ret = misc_register( &gpiotest_misc_dev );
	if (ret)
		dev_err(&pdev->dev, "=== Unable to register misc device\n");

	// allocate chdev read and write bufs
	data_read = devm_kzalloc(&pdev->dev, MAX_BUF, GFP_KERNEL);
	if (!data_read)
		return -ENOMEM;
	data_write = devm_kzalloc(&pdev->dev, MAX_BUF, GFP_KERNEL);
	if (!data_write)
		return -ENOMEM;

	return ret;
}

static int gpiotest_remove(struct platform_device *pdev)
{
	dev_info(&pdev->dev, ": remove called");
	misc_deregister(&gpiotest_misc_dev);

	return 0;
}

static int __init test_init(void)
{
	int status;

	pr_info(DEVICE_NAME ": module init called");

	gpiotest_device = platform_device_alloc("gpiotest", 0);
	if (gpiotest_device == NULL) {
		pr_err(DEVICE_NAME ": failed to allocate device");
		return -EINVAL;
	}

	status = platform_device_add(gpiotest_device);
	if (status) {
		pr_err(DEVICE_NAME ": failed to add device");
		return status;
	}

	status = platform_driver_register(&gpiotest_driver);
	if (status) {
		pr_err(DEVICE_NAME ": failed to register a driver");
		return status;
	}

	return status;
}

static void __exit test_exit(void)
{
	pr_info(DEVICE_NAME ": module exit called");
	platform_driver_unregister(&gpiotest_driver);
	platform_device_del(gpiotest_device);
}

module_init(test_init);
module_exit(test_exit);

MODULE_AUTHOR("Dmytro Semenets <dmitry.semenets@gmail.com>");
MODULE_DESCRIPTION("Gpiotest module");
MODULE_LICENSE("GPL v2");
